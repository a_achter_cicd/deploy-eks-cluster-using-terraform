resource "kubernetes_service" "mongo" {
  metadata {
    name      = "mongo"
    namespace = "sample-apps"
  }

  spec {
    port {
      port        = 27017
      target_port = "27017"
    }

    selector = {
      app = "mongo"
    }
  }
}

resource "kubernetes_deployment" "mongo" {
  metadata {
    name      = "mongo"
    namespace = "sample-apps"
  }

  spec {
    selector {
      match_labels = {
        app = "mongo"
      }
    }

    template {
      metadata {
        labels = {
          app = "mongo"
        }
      }

      spec {
        container {
          name  = "mongo"
          image = "mongo:3.6.17-xenial"

          port {
            container_port = 27017
          }
        }
      }
    }
  }
}

