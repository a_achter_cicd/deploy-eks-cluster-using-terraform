resource "kubernetes_service" "knote" {
  metadata {
    name      = "knote"
    namespace = "sample-apps"
  }

  spec {
    port {
      port        = 80
      target_port = "3000"
    }

    selector = {
      app = "knote"
    }

    type = "LoadBalancer"
  }
}

resource "kubernetes_deployment" "knote" {
  metadata {
    name      = "knote"
    namespace = "sample-apps"
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "knote"
      }
    }

    template {
      metadata {
        labels = {
          app = "knote"
        }
      }

      spec {
        container {
          name  = "knote"
          image = "learnk8s/knote-js:1.0.0"

          port {
            container_port = 3000
          }

          env {
            name  = "MONGO_URL"
            value = "mongodb://mongo:27017/dev"
          }

          image_pull_policy = "Always"
        }
      }
    }
  }
}
output "lb_ip_knote" {
  value = kubernetes_service.knote.load_balancer_ingress[0].hostname
}
