#cluster_name            = "albert-my-app-eks"
#cluster_name = replace("albert-dev-${timestamp()}", ":", "-")
iac_environment_tag     = "development"
name_prefix             = "my-app"
main_network_block      = "10.0.0.0/16"
subnet_prefix_extension = 4
zone_offset             = 8
