resource "kubernetes_deployment" "hellokubernetes" {
  metadata {
    namespace = "sample-apps"
    name      = "hellokubernetes"
    labels = {
      App = "HelloKubernetes"
    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        App = "HelloKubernetes"
      }
    }
    template {
      metadata {
        labels = {
          App = "HelloKubernetes"
        }
      }
      spec {
        container {
          image = "paulbouwer/hello-kubernetes:1.8"
          name  = "sample-hello"

          port {
            container_port = 8080
          }
        }
      }
    }
  }
}
resource "kubernetes_service" "hellokubernetes" {
  metadata {
    namespace = "sample-apps"
    name      = "hellokubernetes"
  }
  spec {
    selector = {
      App = kubernetes_deployment.hellokubernetes.spec.0.template.0.metadata[0].labels.App
    }
    port {
      port        = 80
      target_port = 8080
    }

    type = "LoadBalancer"
  }
}
output "lb_ip_2" {
  value = kubernetes_service.hellokubernetes.load_balancer_ingress[0].hostname
}

